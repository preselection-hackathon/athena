#include "RpcRawDataMonitoring/RpcLv1RawDataValAlg.h"
#include "RpcRawDataMonitoring/RpcLv1RawDataSectorLogic.h"
#include "RpcRawDataMonitoring/RpcLv1RawDataEfficiency.h"
#include "RpcRawDataMonitoring/RPCStandaloneTracksMon.h"
#include "RpcRawDataMonitoring/RpcTrackAnaAlg.h"
#include "RpcRawDataMonitoring/RPCLv1AnaAlg.h"
  

DECLARE_COMPONENT( RpcLv1RawDataValAlg )
DECLARE_COMPONENT( RpcLv1RawDataSectorLogic )
DECLARE_COMPONENT( RpcLv1RawDataEfficiency )
DECLARE_COMPONENT( RPCStandaloneTracksMon )
DECLARE_COMPONENT( RpcTrackAnaAlg )
DECLARE_COMPONENT( RPCLv1AnaAlg )
